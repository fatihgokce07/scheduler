## next runtime
select s.*,CASE  
           WHEN s.lastRunDate is not null
               THEN datetime(date('now', 'localtime'),'+'||s.hourlyMinute || ' minutes')
           ELSE datetime(date('now', 'localtime'),'+'||s.hourlyMinute || ' minutes')
       END nextRunDate
from scheduler s
where type=1 and enabled=1

## version 2 hourly
select * from(
select s.*, 
datetime(datetime(date('now','localtime'),'+'||cast(strftime('%H', datetime('now', 'localtime')) as number)||' hours'),'+'||s.hourlyMinute || ' minutes')
 as nextRunDate
from scheduler s
where type=1 and enabled=1) as f
where
f.lastRunDate is null  or (datetime(f.nextRunDate) < datetime(f.lastRunDate) )

## get weekly sql
select s.*, 
date('now', 'weekday 0',(s.hourlyMinute-7)|| ' days') as nextRunDate
from scheduler s
where type=3 and enabled=1 and (date(nextRunDate)=date('now','localtime') and (date(lastRunDate)!=date(nextRunDate) or lastRunDate is null) )

## haftanin kacinci gununde oldugunu verir
SELECT strftime('%d',date('now', 'weekday 0', '0 days'))-strftime('%d',date('now', 'localtime'));

## haftanin ilk gunu pazartesiyi verir

SELECT strftime('%d',date('now', 'weekday 0', '-6 days'));

## daily sql 
select * from(
select s.*, 
datetime(datetime(date('now','localtime'),'+'||cast(substr(s.hourlyMinute,1,2) as number)||' hours'),'+'||substr(s.hourlyMinute,4,2) || ' minutes')
 as nextRunDate
from scheduler s
where type=2 and enabled=1) as f
where
f.lastRunDate is null  or (datetime(f.nextRunDate) > datetime(f.lastRunDate) )

## sql
select * from(
select s.*,
case type
when 1 then
datetime(datetime(date('now','localtime'),'+'||cast(strftime('%H', datetime('now', 'localtime')) as number)||' hours'),'+'||s.hourlyMinute || ' minutes')
when 2 then
datetime(datetime(date('now','localtime'),'+'||cast(substr(s.hourlyMinute,1,2) as number)||' hours'),'+'||substr(s.hourlyMinute,4,2) || ' minutes')
else
date('now', 'weekday 0',(s.hourlyMinute-7)|| ' days')
end nextRunDate
from scheduler s
where enabled=1) as f
where
f.lastRunDate is null  or (datetime(f.nextRunDate) > datetime(f.lastRunDate) )
## get_request with timeout
async fn get_request_with_delay()->Result<()>{
    use std::time::Duration;

    let client = reqwest::Client::builder().timeout(Duration::from_secs(10)).build()?;
    
    let mut res = client.get("https://cogecomedia.leanstream.co/CKBEFM-MP3").send().await?;
    println!("res:{:?}",res);
    while let Some(chunk) = res.chunk().await? {
        println!("Chunk: {:?}", chunk.to_vec());
        break;
    }
    Ok(())
}