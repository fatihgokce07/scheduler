//use sqlx::sqlite::SqliteRow;
use sqlx::{sqlite::SqliteRow,Pool, Row, Sqlite};
use chrono::prelude::*;
use std::{convert::{TryInto}, sync::Arc};
use crate::models::*;

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

pub struct Repository{
    db:Pool<Sqlite>
}

impl Repository {
    pub async fn new()->Result<Self>{
        let pool = sqlx::SqlitePool::connect("scheduler.db").await?;
        Ok(Repository{
            db:pool
        })
    }
    pub async fn update_last_rundate(&self,scheduler_id:i64)->Result<()>{
        let mut conn = self.db.acquire().await?;
        let dt: DateTime<Local> = Local::now();
        let parse = dt.format("%Y-%m-%d %H:%M:%S").to_string();
       
        let _ = sqlx::query(format!("update scheduler set lastRunDate='{}' where id={}",parse,scheduler_id).as_str()).execute(&mut conn).await?;
        Ok(())
    }
    pub async fn insert_country(&self,name:String)->Result<()>{
        let mut conn = self.db.acquire().await?;
        let id = sqlx::query(&*format!("insert into Country(name)values('{}')",name)).execute(&mut conn).await?.last_insert_rowid();
        println!("{} {}",name,id);
        Ok(())
    }
    pub async fn get_scheduler_list(&self)->Result<Vec<Scheduler>>{
        let mut conn = self.db.acquire().await?;
        //let mut v = vec![];
        let mut datas = sqlx::query("select * from SchedulersView")
              .try_map(|row:SqliteRow|{

                  let id:i64 = row.try_get(0).unwrap();
                  let name:String = row.try_get(1).unwrap();
                  let st:u8 = row.try_get(2).unwrap();
                  let et:SchedulerType = st.try_into().unwrap();
                  let date_str:String = row.try_get(3).unwrap();
                  let last_run_date = match date_str.is_empty() {
                      false => Some(NaiveDateTime::parse_from_str(&*date_str, "%Y-%m-%d %H:%M:%S").unwrap()),
                      _ => None
                  };
                  let val:String = row.try_get(4).unwrap();
                  let enabled:bool = row.try_get(5).unwrap();
                  let date_str:String = row.try_get(6).unwrap();
                  let nex_run_date = match date_str.is_empty() {
                      false => Some(NaiveDateTime::parse_from_str(&*date_str, "%Y-%m-%d %H:%M:%S").unwrap()),
                      _ => None
                  };  
                  
                  let d = Scheduler{
                      id:id,name:name,scheduler_type:et,last_run_date:last_run_date,value:val,
                      enabled:enabled,next_run_date:nex_run_date
                  };
                  Ok(d)
              })
              .fetch_all(&mut conn).await?;
        
    
        Ok(datas)
    }
}

pub struct Db(pub sqlx::Pool<Sqlite>);