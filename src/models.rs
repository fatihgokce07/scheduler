use std::convert::{TryFrom, TryInto};
use chrono::prelude::*;
#[derive(Debug,PartialEq)]
pub enum SchedulerType {
    Horly=1,
    Daily,Weekly
}
#[derive(Debug)]        
pub struct Scheduler{
    pub id:i64,
    pub name:String,
    pub scheduler_type:SchedulerType,
    pub last_run_date:Option<NaiveDateTime>,
    pub value:String,
    pub enabled:bool,
    pub next_run_date:Option<NaiveDateTime>
}
impl TryFrom<u8> for SchedulerType {
    type Error = &'static str;

    fn try_from(value: u8) -> std::result::Result<Self, Self::Error> {
        match value {
            x if x == SchedulerType::Horly as u8 =>Ok(SchedulerType::Horly),
            2 => Ok(SchedulerType::Daily),
            3 => Ok(SchedulerType::Weekly),
            _ => Err("cannot found scheduler type")
        }
    }
}