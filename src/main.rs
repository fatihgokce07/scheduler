use std::time;
use std::{collections::HashMap, thread};
use std::fmt::format;
use std::process::Command;
use std::sync::Arc;
use tokio::sync::mpsc::{channel, Sender};
mod models;
mod repository;
use repository::*;
use models::*;
#[tokio::main]
async fn main() -> Result<()>{    
    
    let rep = Repository::new().await?;
    let repo = Arc::new(Repository::new().await?);
      
    loop {
        println!("working");
        let mut treads = vec![];  
        let schedulers:Vec<Scheduler> = repo.get_scheduler_list().await?; 
        println!("count:{}",schedulers.len());
        for s in schedulers{          
            let db = Arc::clone(&repo);
            let t = tokio::spawn(async move{
                //run_scheduler(s, db).await;                
                let _ = db.insert_country(s.name).await;
             
            });
            treads.push(t);
            
        }
        for t in treads{
            let _ = t.await;            
        }
        println!("sleeping");
        tokio::time::sleep(tokio::time::Duration::from_millis(20000)).await;
        //thread::sleep(time::Duration::from_millis(5000));
    }
    Ok(())
}
pub async fn run_scheduler(scheduler:Scheduler,db:Arc<Repository>)->Result<()>{
    println!("running scheduler:{}",scheduler.name);
    let r = db.update_last_rundate(scheduler.id).await;
    println!("run scheduler res:{:?}",r);
    Ok(())
}

